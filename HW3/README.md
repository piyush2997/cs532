#  Homework 3 Assignment


##Objective:
To add additional features to the search program implemented in Homework #2

## Steps Involved:
1. Flag all the arguments provided.
2. While traversing through all the files filter the arguments provided along with unix commands accordingly. 
3. Since, -E is to be implemented at last, as in the final files should be treated as a single input for the UNIX command provided, so that the UNIX command is only run once at the end of program.


## Steps to run the program.
Compile : 
gcc Piyush_CS532_HW3.c -o piyush

./hw3 <arguments>
Example: 
    $ ./piyush -s 1024 -e "ls -l"
    or 
    $ ./piyush -f wri -E "tar cvf test.tar"
    
    
