#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include<sys/wait.h>
#include <unistd.h>
#include <string.h>
/*
Name: Piyush Soni
BlazerId: psoni
Project #: Homework 3
To compile: gcc hw3.c -o hw3
To run: ./hw3 <commands and arguments> <directory>
ex: ./hw3 -f txt -S -s 100 -t f ./projects/ -e or -E "unix command"
*/

typedef struct
{
    int S_flag;
    int s_flag;
    int f_flag;
    int t_flag;
    int fileSize;
    char filterTerm[300];
    char fileType[2];
    char unix[100];
    int e_flag;
    int E_flag;
} FlagArgs;


typedef void FileHandler(char *filePath, char *dirfile, FlagArgs flagArgs, int nestingCount);

char **unix_args;
char args[2056];
int array_counter = 0;


void myPrinterFunction(char *filePath, char *dirfile, FlagArgs flagArgs, int nestingCount)
{
    struct stat buf;
    lstat(filePath, &buf);
    char line[100];
    strcpy(line, "");
    strcat(line, dirfile);
    if (flagArgs.S_flag)
    {
        char strsize[10];
        sprintf(strsize, " %d", (int)buf.st_size);
        strcat(line, strsize);
    }
    if (flagArgs.s_flag)
    {
        if (flagArgs.fileSize > (int)buf.st_size)
        {
            strcpy(line, "");
        }
    }
    if (flagArgs.f_flag)
    {
        if (strstr(dirfile, flagArgs.filterTerm) == NULL)
        {
            strcpy(line, "");
        }
    }
    if (flagArgs.t_flag)
    {
        if (strcmp(flagArgs.fileType, "f") == 0)
        {
            if (S_ISDIR(buf.st_mode) != 0)
            {
                strcpy(line, "");
            }
        }
        if (strcmp(flagArgs.fileType, "d") == 0)
        {
            if (S_ISREG(buf.st_mode) != 0)
            {
                strcpy(line, "");
            }
        }
    }

    if (strcmp(line, "") != 0)
    {
         int i = 0;
        for (i = 0; i <= nestingCount; i++)
        {
            printf("\t");
        }
        printf("%s\n", line);
        if (flagArgs.e_flag)
        {
            sprintf(line, "%s %s", flagArgs.unix, filePath);
            unix_args[array_counter] = filePath;
            int pid_e = fork();
            if (pid_e == 0)
            {
                execvp(unix_args[0], unix_args);
            }
            else if (pid_e > 0)
            {
                wait(NULL);
            }
        }
        if (flagArgs.E_flag)
        {
            sprintf(args, "%s %s", args, filePath);
        }
    }
}

void readFileHierarchy(char *dirname, int nestingCount, FileHandler *fileHandlerFunction, FlagArgs flagArgs)
{
    struct dirent *dirent;
    DIR *parentDir = opendir(dirname);
    if (parentDir == NULL)
    {
        printf("Error opening directory '%s'\n", dirname);
        exit(-1);
    }
    while ((dirent = readdir(parentDir)) != NULL)
    {
        if (strcmp((*dirent).d_name, "..") != 0 &&
            strcmp((*dirent).d_name, ".") != 0)
        {
            char pathToFile[300];
            sprintf(pathToFile, "%s/%s", dirname, ((*dirent).d_name));
            fileHandlerFunction(pathToFile, (*dirent).d_name, flagArgs, nestingCount);
            if ((*dirent).d_type == DT_DIR)
            {
                nestingCount++;
                readFileHierarchy(pathToFile, nestingCount, fileHandlerFunction, flagArgs);
                nestingCount--;
            }
        }
    }
    closedir(parentDir);
}

int main(int argc, char **argv)
{
    unix_args = (char**) malloc (sizeof(char));
    int opt = 0;
    FlagArgs flagArgs = {
        .S_flag = 0,
        .e_flag = 0,
        .E_flag = 0,
        .s_flag = 0,
        .f_flag = 0,
        .t_flag = 0};

    while ((opt = getopt(argc, argv, "Ss:f:t:e:E:")) != -1)
    {
        switch (opt)
        {
        case 'S':
            flagArgs.S_flag = 1;
            break;
        case 's':
            flagArgs.s_flag = 1;
            flagArgs.fileSize = atoi(optarg);
            break;

        case 'f':
            flagArgs.f_flag = 1;
            strcpy(flagArgs.filterTerm, optarg);
            break;

        case 't':
            flagArgs.t_flag = 1;
            strcpy(flagArgs.fileType, optarg);
            break;
        case 'e':
            flagArgs.e_flag = 1;
            strcpy(flagArgs.unix, optarg);
            break;
        case 'E':
            flagArgs.E_flag = 1;
            strcpy(flagArgs.unix, optarg);
            
            break;
        }
    }

    char *u_args = strtok(optarg, " ");
    while (u_args != NULL)
    {
        unix_args[array_counter++] = u_args;
        u_args = strtok(NULL, " ");
    }
    
    if (opendir(argv[argc - 1]) == NULL)
    {
        char defaultdrive[300];
        getcwd(defaultdrive, 300);
        printf("%s\n", defaultdrive);
        readFileHierarchy(defaultdrive, 0, myPrinterFunction, flagArgs);
        if (flagArgs.E_flag)
        {
            char *token = strtok(args, " ");
            while (token != NULL)
            {
                unix_args[array_counter] = token;
                token = strtok(NULL, " ");
                array_counter++;
            }
            int pid_E = fork();
            if (pid_E == 0)
            {
                execvp(unix_args[0], unix_args);
            }
            else if (pid_E > 0)
            {
                wait(NULL);
            }
        }
        return 0;
    }
    printf("%s\n", argv[argc - 1]); 
    readFileHierarchy(argv[argc - 1], 0, myPrinterFunction, flagArgs);
    return 0;
}
