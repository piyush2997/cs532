//
//  HW2.c
//
//
//  Created by Piyush Soni on 2/17/22.
//
/*
Name: Piyush Soni
BlazerId: psoni
Project #: HW2
To compile: gcc HW2.c
To run: ./a.out <command-line arguments>
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

char buf[100];
char *temp;
char *file_type;
struct stat sb;
int command = 0;
int ip_size;
char *x;
typedef int MYFUNC(char *file, int flag, int ip_size, int command);
int traversal(char *file, int flag, int ip_size, int command){

    char childDir[1000];
    struct dirent *dirent;
    DIR *parentDir;
    if(strcmp(file, ".")==0){
        parentDir = opendir(".");
    }
    else{
        parentDir = opendir(file);
    }
    if (flag!=0){
        
    }
    if (!parentDir)
        return flag;
    while ((dirent = readdir(parentDir)) != NULL)
    {
        if (strcmp((*dirent).d_name, ".") != 0 && strcmp((*dirent).d_name, "..") != 0)
        {

            stat((*dirent).d_name, &sb);
            switch (command){
                case 1:
                    x = strstr((*dirent).d_name, file_type);
                    if(x){
                        printf("%*s%s \n", flag, "", (*dirent).d_name);
                    }
                    break;
                    
                case 10:
                    if (ip_size<sb.st_size){
                        printf("%*s%s \n", flag, "", (*dirent).d_name);
                        break;
                    }
                    
                case 11:
                    x = strstr((*dirent).d_name, file_type);
                    if((x!=NULL) && (ip_size<sb.st_size)){
                            printf("%*s%s \n", flag, "", (*dirent).d_name);
                            break;
                        }
                    
                case 101:
                    x = strstr((*dirent).d_name, file_type);
                    if(x){
                        printf("%*s%s (%lld bytes)\n", flag, "", (*dirent).d_name, (long long) sb.st_size);
                    }
                    break;
                    
                case 110:
                    if (ip_size<sb.st_size){
                        printf("%*s%s (%lld bytes)\n", flag, "", (*dirent).d_name, (long long) sb.st_size);
                        break;
                    }
                case 111:
                    x = strstr((*dirent).d_name, file_type);
                    if((x) && (sb.st_size>ip_size)){
                        printf("%*s%s (%lld bytes)\n", flag, "", (*dirent).d_name, (long long) sb.st_size);
                            break;
                        }
                    else{
                        break;
                    }
                    
                default:
                    printf("%*s%s (%lld bytes)\n", flag, "", (*dirent).d_name, (long long) sb.st_size);
                    break;

            }
            if ((*dirent).d_type == DT_LNK){
                realpath((*dirent).d_name, buf);
                temp = strrchr(buf, '/') + 1;
                printf("%s (%s)\n", (*dirent).d_name, temp);
                continue;
            }
            strcpy(childDir, file);
            strcat(childDir, "/");
            strcat(childDir, (*dirent).d_name);
            flag = traversal(childDir, flag+=2, ip_size, command);
            flag-=2;
        }
    }
    closedir(parentDir);
    printf("\n");
    return flag;
}


int opfunc(char *file, int flag, int ip_size, int command, MYFUNC *f) {
    return f(file, flag, ip_size, command);
}

int main (int argc, char **argv) {
    int opt;
    int index;
    int count = 1;
    while((opt = getopt(argc, argv, "Sfs:")) != -1){
        switch (opt){
            case 'S':
                command = command + 100;
                break;
                
            case 's':
                command = command + 10;
                ip_size = atoi(argv[count+1]);
                break;
                
            case 'f':
                command = command + 1;
                break;
                
            default:
                command = 100;
                break;
        }
        count++;
    }
    for(; optind < argc; optind++){
        file_type = argv[optind];
        }
    
    char *dir;
    dir = ".";

    int flag = opfunc(dir, 0, ip_size, command, traversal);

    return 0;
}
