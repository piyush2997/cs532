//
//  HW2.c
//  
//
//  Created by Piyush Soni on 2/17/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

char buf[100];
char *temp;


void s_arg(char *file, int ip_size) {
    
    struct dirent *dirent;
    DIR *parentDir;
    
    while((dirent = readdir(parentDir)) != NULL){
        printf ("[%d] %s\n", count, (*dirent).d_name);
        count++;
    }
    closedir (parentDir);
}




int traversal(char *file, int flag)
{
    char childDir[1000];
    struct dirent *dirent;
    DIR *parentDir;
    if(strcmp(file, ".")==0){
        parentDir = opendir(".");
    }
    else{
        parentDir = opendir(file);
    }
    
    if (!parentDir)
        return flag;
    
    
    while ((dirent = readdir(parentDir)) != NULL)
    {
        if (strcmp((*dirent).d_name, ".") != 0 && strcmp((*dirent).d_name, "..") != 0)
        {
            if ((*dirent).d_type == DT_LNK){
                realpath((*dirent).d_name, buf);
                temp = strrchr(buf, '/') + 1;
                printf("%s <%s>\n", (*dirent).d_name, temp);
                continue;
            }
            printf("%*s%s\n", flag, "", (*dirent).d_name);
            strcpy(childDir, file);
            strcat(childDir, "/");
            strcat(childDir, (*dirent).d_name);
            flag = traversal(childDir, flag+=2);
            flag-=2;
        }
    }
    closedir(parentDir);
    printf("\n");
    return flag;
}

int opfunc(char *file, int flag, int ip_size, int command, MYFUNC *f) {
    return f(file, flag, ip_size, command);
}

int main (int argc, char **argv) {
    int opt;
    int index;
    int count = 1;
    while((opt = getopt(argc, argv, "Sfs:")) != -1){
        switch (opt){
            case 'S':
                command = command + 100;
                break;
                
            case 's':
                command = command + 10;
                ip_size = atoi(argv[count+1]);
                break;
                
            case 'f':
                command = command + 1;
                break;
                
            default:
                command = 100;
                break;
        }
        count++;
    }
    for(; optind < argc; optind++){
        file_type = argv[optind];
        }
    
    char *dir;
    dir = ".";

    int flag = opfunc(dir, 0, ip_size, command, traversal);

    return 0;
}
