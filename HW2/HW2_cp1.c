//
//  HW2.c
//  
//
//  Created by Piyush Soni on 2/17/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>


int traversal(char *file, int flag)
{
    char childDir[1000];
    struct dirent *dirent;
    DIR *parentDir;
    if(strcmp(file, ".")==0){
        parentDir = opendir(".");
    }
    else{
        parentDir = opendir(file);
    }
    
    
    if (!parentDir)
        return flag;
    while ((dirent = readdir(parentDir)) != NULL)
    {
        if (strcmp((*dirent).d_name, ".") != 0 && strcmp((*dirent).d_name, "..") != 0)
        {
            printf("%*s%s\n", flag, "", (*dirent).d_name);
            strcpy(childDir, file);
            strcat(childDir, "/");
            strcat(childDir, (*dirent).d_name);
            flag = traversal(childDir, flag+=2);
            flag-=2;
        }
    }
    closedir(parentDir);
    printf("\n");
    return flag;
}

int S (int argc, char **argv) {
    char *dir;
    if (argc ==1) {
        dir = ".";
    }
    else{
        dir = argv[1];
    }
    if (!opendir(dir)){
        printf ("%s - Should be a directory\n", argv[1]);
        exit(-1);
    }
    int flag = traversal(dir, 0);
    return 0;
}


int main (int argc, char **argv) {
    S(argc, argv);
}
