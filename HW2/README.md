#  HW 02 Assignment


##Objective:
To implement a search program in C program using system calls for files and directories.

    

## Steps to run the program.

**Argument:** ./a.out <command_arguments>
The program supports three command-line options: 
1. -S 
This should list all files in the file hierarchy and print the file size next to the filename in 
parenthesis.  
2. -s <file size in bytes> 
This should list all files in the file hierarchy with file size greater than or equal to the 
value specified. 
3. -f <string pattern> 
This should list all files in the file hierarchy whose file name or directory name contains 
the substring specified in the string pattern option.

 
