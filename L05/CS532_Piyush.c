//
//  L05.c
//
//
//  Created by Piyush Soni on 2/12/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>


void traversal(char *file)
{
    char childDir[1000];
    struct dirent *dirent;
    DIR *parentDir = opendir(file);

    if (!parentDir)
        return;
//    printf("\n%s: Directory\n", file);
    while ((dirent = readdir(parentDir)) != NULL)
    {
        printf("%s: ", file);
        if (strcmp((*dirent).d_name, ".") != 0 && strcmp((*dirent).d_name, "..") != 0)
        {
            
            printf ("/%s\n", (*dirent).d_name);
            strcpy(childDir, file);
            strcat(childDir, "/");
            strcat(childDir, (*dirent).d_name);

            traversal(childDir);
            
        }
    }
    closedir(parentDir);
    printf("\n");
}

int main (int argc, char **argv) {

    if (argc !=2) {
    printf ("Usage: %s <dirname>\n", argv[0]);
    exit(-1);
    }
    if (!opendir(argv[1])){
        printf ("%s - Should be a directory\n", argv[1]);
        exit(-1);
    }
    traversal(argv[1]);
    return 0;
} 
