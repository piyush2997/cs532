#  Lab 05 Assignment


##Objective:
C program to traverse the file hierarchy recursively and list all the sub directories and the files in these sub directories. 

## Steps Involved:
1. Check if input argument is correct and contains the directory name.
2. Check if directory name provided as valid or not.
3. Call traversal function which prints all the files in a directory.
4. In traversal :
    a. We will again check if its a directory on not because we will be recursively calling the function and we can only traverse directories. Return if not a directory.
    b. We will read the files in directory using **readdir()** function.
    c. Print the file name.
    d. Now make a variable with value "<input_directory>/<file_name>" 
    e. Now we call again call **traversal()** with this path as input. Go to step "a".
    f. close the directory.
    

## Steps to run the program.

**Argument:** ./a.out <directory_name>
