#include<stdlib.h>
#include<string.h>
#include<dirent.h>
#include<stdio.h>
void printdirec(char *);                                           
int main(int argc,char *argv[])
{
    struct dirent *diren;
    DIR *pDir;  
    if (argc < 2) 
    {                                           
        printf ("Usage: %s <dirname>\n", argv[0]);                
        exit (-1);
    }
    pDir = opendir(argv[1]);
    if(pDir==NULL)
    {
      printf("Directory cannot be opened '%s'\n", argv[1]);
      exit(-1);
     }
     else
     {
       printdirec(argv[1]); 
      }
                
}
void printdirec(char *Path)
{
    static int flag=0,count=1;
    char n[1000];                                          
    DIR *pDir;
    struct dirent *diren;              
    pDir= opendir(Path);                                   
    if (pDir == NULL && flag==0)                           
    {
         printf ("Directory cannot be opened'%s'\n",Path);
         exit (-1);
    }
    while ((diren= printdirec(pDir)) != NULL)                
    {
        flag=1;        
        if (strcmp((*diren).d_name, ".") != 0 && strcmp((*diren).d_name, "..") != 0)  
        { 
           printf ("[%d] %s\n", count, (*diren).d_name);       
           count++;
           if((*diren).d_type==DT_DIR)                         
           {
                                                                
                strcpy(n, Path);
                strcat(n, "/");
                strcat(n, (*diren).d_name);
                printdirec(n);                              
           }
        }
     }
     
     closedir(pDir);   
}
