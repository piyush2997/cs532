/* Simple program to illustrate the use of fork-exec-wait pattern.
 * This version uses execvp and command-line arguments to create a new process.
 * To Compile: gcc -Wall forkexecvp.c
 * To Run: ./a.out <command> [args]
 */

/*
Name: Piyush Soni
BlazerId: psoni
Project #: Lab 09
To Compile : gcc Piyush_L09.c -o piyush
To Run : ./piyush <command>
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

pid_t pid;

void sig_parent(int signo)
{
    printf("\nKilling Parent Process\n");
    exit(-1);
}

void sig_handler(int signo)
{
    signal(signo, SIG_IGN);
    switch (signo)
    {
    case SIGINT:
        printf("\nInterupting Process : %d and Parent waiting for quit signal\n", pid);
        kill(pid, SIGINT);
        break;
    case SIGTSTP:
        printf("\nSuspending Process : %d and Parent waiting for quit signal\n", pid);
        kill(pid, SIGTSTP);
        break;
    default:
        printf("Didn't catch that");
    }

    signal(SIGQUIT, sig_parent);
}

int main(int argc, char **argv)
{

    int status;
    pid = getpid();
    if (argc < 2)
    {
        printf("Usage: %s <command> [args]\n", argv[0]);
        exit(-1);
    }

    pid = fork();
    if (pid == 0)
    { /* this is child process */
        execvp(argv[1], &argv[1]);
        printf("If you see this statement then execl failed ;-(\n");
        exit(-1);
    }
    else if (pid > 0)
    { /* this is the parent process */
        printf("Parent PID : %d\n", getpid());
        printf("Wait for the child process to terminate\n");
        printf("Child PID : %d\n", pid);
        if (signal(SIGINT, sig_handler) == SIG_ERR)
        {
            printf("can't catch SIGINT\n");
            exit(-1);
        }
        if (signal(SIGTSTP, sig_handler) == SIG_ERR)
        {
            printf("can't catch SIGINT\n");
            exit(-1);
        }
        wait(&status); /* wait for the child process to terminate */

        if (WIFEXITED(status))
        { /* child process terminated normally */
            printf("Child process exited with status = %d\n", WEXITSTATUS(status));
            
        }

        else
        { /* child process did not terminate normally */
            printf("Child process did not terminate normally! \n");
            if (status == 2)
            {
                for (;;)
                    pause();
            }
            /* look at the man page for wait (man 2 wait) to determine
               how the child process was terminated */
        }
    }
    else
    {                   /* we have an error */
        perror("fork"); /* use perror to print the system error message */
        exit(EXIT_FAILURE);
    }

    printf("[%ld]: Exiting program .....\n", (long)getpid());

    return 0;
}
