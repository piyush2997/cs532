#  Lab 04 Assignment


##Objective:
C program that takes two filename as command-line arguments and 
concatenates the contents of the second file to the first file. 

## Steps Involved:
1. Check if input argument is correct and contains the file names or not, with .txt in their end.
2. Check if both file names are different, print and end if file names are same.
3. Open first file as write only.
4. Open second file as read only.
5. In a while loop read each line from second file. and inside the loop :
    a. set pointer at the end of file.
    b. write and print error if occured while write operation.
6. close both files.

## Steps to run the program.
First file: file to write in.
Second file: file to write from.
*Note: Order is specific, file names should be different and make sure to add .txt in the end of file name.*

**Argument:** ./a.out <first_file>.txt <second_file>.txt

