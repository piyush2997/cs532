//
//  L04.c
//  
//
//  Created by Piyush Soni on 2/6/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define BUFFSIZE 4096

int main(int argc, char** argv) {
    
    char buf[BUFFSIZE];
    ssize_t n;
//    Checking if input given is correct and giving an error if not.
    if ((argc != 3) && (strchr(argv[1], '.txt') && (strchr(argv[2], '.txt')))){
        printf("Usage: %s <first_filename>.txt <second_filename>.txt\n", argv[0]);
        exit (-1);
    }
//    Checking if file names are different or not.
    if ((argc==3) && (strcmp(argv[1], argv[2])==0)){
        printf("Please provide different file names.");
        exit(-1);
    }
//    Opening First file in write mode, since we're going to add the contents of second file to this file
    int file_to_add_in = open(argv[1], O_WRONLY);
//    Opening Second file in read mode, since we're going to just read the contents from this file and not make any changes.
    int file_to_add_from = open(argv[2], O_RDONLY);
//    Reading each line from second file.
    while ((n = read(file_to_add_from, buf, BUFFSIZE)) > 0){
//        Setting pointer to end of first file.
        if (lseek(file_to_add_in, 0, SEEK_END) >= 0){
//            Writing buf(line read from second file) at the pointer location on first file, and printing if any error occured during the write operation.
            if (write(file_to_add_in, buf, strlen(buf)) != strlen(buf)) {
                printf("Error writing to file\n");
            }
        }
    }
//    Closing both files.
    close(file_to_add_in);
    close(file_to_add_from);
}
