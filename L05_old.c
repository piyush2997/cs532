//
//  L05.c
//
//
//  Created by Piyush Soni on 2/15/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <dirent.h>

DIR *parentDir;
struct dirent *dirent;

void inside(char *file){
    parentDir = opendir (file);
    if (parentDir == NULL) {
      printf ("Error opening directory '%s'\n", file);
      exit (-1);
    }
    int count = 1;
    while((dirent = readdir(parentDir)) != NULL){
      printf ("[%d] %s\n", count, (*dirent).d_name);
      count++;
    }
    closedir (parentDir);
}

int main(int argc, char **argv) {
    int i;
    struct stat buf;
    char *ptr;

    for (i = 1; i < argc; i++) {
        printf("%s: \n", argv[i]);
        if (lstat(argv[i], &buf) < 0) {
            printf("lstat error");
            continue;
        }
        if (S_ISDIR(buf.st_mode)){
            inside(argv[i]);
        }
        
        printf("%s\n", ptr);
    }
}
