//
//  A02.c
//  
//
//  Created by Piyush Soni on 1/23/22.
//

#include "A02.h"
#include<stdio.h>

int main()
{
//    int number =0;
//    printf("Enter number of elements");
//    scanf("%d",&number);
//    int m, n, element, x, array[number];
//    for(m=0;m<number;++m){
//        printf("Enter %dth elements", m+1);
//        scanf("%d",&array[m]);
//
//
    int n =0;
    printf("Enter number of elements : \n");
    scanf("%d",&n);
    int i, j, pivot, temp, a[n];
    printf("Elements : ");
    for(i=0;i<n;i++){
        scanf("%d",&a[i]);
    }
    
    
    for(i=1;i<n;++i){
        pivot = a[i];
        j = i-1;
        while (j >= 0){
            if (a[j]>pivot){
                temp = a[j];
                a[j] = a[i];
                a[i] = temp;
                i = j;
            }
            --j;
        }
    }
    printf("Sorted array :\n");
    for(i=0;i<n;++i){
        printf("%d \n", a[i]);
    }
}
