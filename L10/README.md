#  Lab 10


##Objective:
Modify the program lab07_solution.c such that output and error to <pid>.out and <pid>.err, where pid refers to the pid of the child process.

## Steps Involved:
1. open the <file_name>.txt which contains the list of command line arguments.
2. when entering the child process create 2 files with pid of the child process as name:
    a. <pid>.out to redirect the output of the child process.
    b. <pid>.err to redirect the error of the child process. 


## Steps to run the program.
Compile : 
gcc Piyush_L10.c -o piyush

<file_name>.txt :- the text file containing the list of command line arguments. 
./hw3 <file_name>.txt
Example: 
    $ ./piyush stdin.txt
    
    
