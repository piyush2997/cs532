//
//  A03.c
//  
//
//  Created by Piyush Soni on 1/29/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

// Creating Structure for keywords named "keywords".
struct keywords{
    char *word;
    int count;
};

// Globally initializing i used in "for" loop to avoid declarating it multiple times.
int i;

// Function to check and update the count of keywords accurence in a given string.
void occurence(struct keywords *kw_table, char *str, int argc){
    char *token;
    int x;
    
//    Delimited set as space " "
    const char s[2] = " ";
    
//    Generating first token
    token = strtok(str, s);
    
//    While loop to iterate till last token.
    while (token!=NULL){
        
//        For loop to compare token with keywords.
        for (i=1; i<argc;i++){
            
//            Comparing keyword and token.
            x = strcmp(kw_table[i].word, token);
            if (x==0){
                
//                Updating count if keyword and token are same (0 -> same).
                kw_table[i].count = kw_table[i].count + 1;
            }
        }
        
//        Moving to next token.
        token = strtok(NULL, s);
    }
}

//Function to remove "." and "," from the sentence if present
void remove_character(char* str, char c)
{
 
    int j, n = strlen(str);
    for (i = j = 0; i < n; i++)
        if (str[i] != c)
            str[j++] = str[i];
 
    str[j] = '\0';
}

//Function to print a structure.
void print_table(struct keywords *kw_table, int argc){
    printf("Keyword -> Occurence\n");
    for (i = 1; i<argc; i++){
        printf("%s -> %d\n", kw_table[i].word, kw_table[i].count);
    }
}

int main(int argc, char *kw[]) {
    
//    Dynamic memory allocation for kw_table in struct keywords
    struct keywords *kw_table = malloc(argc* sizeof(struct keywords));

//    Inserting keywords from command line to kw_table and initializing count for each keyword to 0.
    for (i = 1; i<argc; i++){
        kw_table[i].word = kw[i];
        kw_table[i].count = 0;
//        printf("%s, %d\n", kw_table[i].word, kw_table[i].count);
    }

    char *buffer = NULL;
    size_t len = 0;
    
//    Taking each line from the command line or input text file.
//    Buffer -> each line
    while (getline(&buffer, &len, stdin) > 0){

//        printf("%s", buffer);
//        Removing characters from line.
        remove_character(buffer, '.');
        remove_character(buffer, ',');
        
//        Checking keyword occurence and updating count.
        occurence(kw_table, buffer, argc);
    }
    
//    Print kw_table to display count of each keyword.
    print_table(kw_table, argc);
    
//    Free allocated memory.
    free(buffer);

}


