# ReadMe  

## Objectives
To implement a C program that uses structures, dynamic memory allocation, and
string functions provided by the C library.

## Description
The goal of this project is to implement a C program that a variable number of
keywords as command line argument, reads text from the standard input stream,
searches the keywords in the input stream, and when the end of input stream is
reached prints the number of times each keyword appears in the input text.

## Example
### Input : 
$ ./a.out an and or to
<br>This is some sample text. You have to find out how many times certain keyword
s appear.
<br>It is possible that some words may not appear at all while some words might a
ppear multiple times.
<br>There could be newlines and white spaces and long lines. You should keep acce
pting the text until the end of file character is found. You can use the exam
ple program getline.c to figure out how to read line by line until there are
no more characters in the input stream.
### Output:
Here is the number of times each keyword appears:
<br>an: 0
<br>and: 2
<br>or: 0
<br>to: 3

## Functions:
### 1. getline()
**getline(&buffer,&size,stdin);**
<br>

The getline() function is prototyped in the stdio.h header file. Here are the three arguments:

**&buffer** is the address of the first character position where the input string will be stored. It’s not the base address of the buffer, but of the first character in the buffer. This pointer type (a pointer-pointer or the ** thing) causes massive confusion.

**&size** is the address of the variable that holds the size of the input buffer, another pointer.

**stdin** is the input file handle. So you could use getline() to read a line of text from a file, but when stdin is specified, standard input is read.

### 2. occurence(struct keywords *kw_table, char *str, int argc)
<br> Updates the number of times the keywords in kw_table struct field appear in given string str. argc is the number of characters passed in command-line. Since, we used that for creating the struct. It tokenizes the line and then compares each keyword to each token.

### 3. remove_character(char* str, char c)

Input string from which you want to remove character and character which you want to remove.  

## Steps:

1. Take input the keywords and text/text_file to read.
2. Insert keywords in struct.
3. read each line using get_line in a while loop till it reaches the last line.
    <br>a. remove "." and "," characters to avoid missing some keyword occurences, example: keyword : best, line : He was the best.
    <br>b. call ocurence()
4. Print struct containing keywords and its count using print_table().

## To run File:
1. gcc A03.c
2. ./a.out <desired keywords> < <text_file_name.txt>
<br>    Example: ./a.out it the an and < tale.txt

*Note:*

Make sure all the files are in the same location as A03.c
