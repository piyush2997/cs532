/* 
  Simple Pthread Program to find the sum of a vector.
  Uses mutex locks to update the global sum. 
  Author: Purushotham Bangalore
  Date: Jan 25, 2009

  To Compile: gcc -O -Wall Piyush_CS532_L12.c -lpthread -o piyush
  To Run: ./piyush.out 1000 4
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;

typedef struct foo {
    double sum;
    int N;
    int size;
    long tid;
    pthread_t ptid;
    double *a;
} FOO;

void *compute(void *arg) {
    int myStart, myEnd, myN, i;
    FOO *info = (FOO *)arg;
    
    // determine start and end of computation for the current thread
    myN = info->N/info->size;
    myStart = info->tid*myN;
    myEnd = myStart + myN;
//    printf("%d, %d, %d\n", myStart, myEnd, myN);
    if (info->tid == (info->size-1)) {
        myEnd = info->N;
    }

    // compute partial sum
    double mysum = 0.0;
    for (i=myStart; i<myEnd; i++){
        mysum += (info->a)[i];
    }
    
    // grab the lock, update global sum, and release lock
    pthread_mutex_lock(&mutex);
    info->sum = info->sum + mysum;
    pthread_mutex_unlock(&mutex);
    return (NULL);
}


int main(int argc, char **argv) {
    long i;
    double *a = NULL, sum = 0.0;
    int N, size;
    FOO *info = NULL;
    
    if (argc != 3) {
       printf("Usage: %s <# of elements> <# of threads>\n",argv[0]);
       exit(-1);
    }

    N = atoi(argv[1]); // no. of elements
    size = atoi(argv[2]); // no. of threads

    // allocate structure
    info = (FOO *)malloc(sizeof(FOO)*size);
    
    // allocate vector and initialize
    a = (double *)malloc(sizeof(double)*N);
    for (i=0; i<N; i++)
        a[i] = (double)(i + 1);

    // create threads
    for ( i = 0; i < size; i++)
    {
        info[i].N = N;
        info[i].size = size;
        info[i].tid = i;
        info[i].a = a;
        pthread_create(&info[i].ptid, NULL, compute, (void *)&info[i]);
    }

    // wait for them to complete
    for ( i = 0; i < size; i++)
        pthread_join(info[i].ptid, NULL);

    // sum each thread's sum
    for ( i = 0; i < size; i++)
        sum = info[i].sum + sum;
    
    printf("The total is %g, it should be equal to %g\n", sum, ((double)N*(N+1))/2);
    
    free(info);
    return 0;
}

