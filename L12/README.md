#  Lab 12


##Objective:
Modify the pthread_sum.c program to create a structure and pass the structure as argument to the thread creation function instead of using global variables 

## Steps Involved:
1. Create structure and push the values to structure instead of global variables.

## Steps to run the program.
Compile : 
  gcc -O -Wall Piyush_CS532_L12.c -lpthread -o piyush 
    ./piyush <number of elements> <number of threads>
Example: 
    $ ./piyush 1000 8
    
    
