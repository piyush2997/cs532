//
//  HW3.c
//
//
//  Created by Piyush Soni on 3/10/22.
//

//P1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main (int argc, char **argv){
    printf("hello frommp1, proces id = () %d\n"), getpid());
    int pid = fork();
    int status;
    if (pid == 0){
        char *args[]= {"Hello", "CS", "332", NULL };
        execv(path: "./p2", "Hello CS 332");
        printf("We are not supposed to see this");
    }
    else if (pid > 0){
        wait (&status);
    }
    return 0;
}

// P2

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char ** argv){
    printf("Hello from p2, process id = () %d\n", getpid());
    printf("the argumanets are %s %s %s\n", argv[0], argv [1], argv[2]);
    printf("Now, the child process will terminate\n");
    return 0;
}

//Token

#include <stdio.h>
#include <string.h>
 
int main()
{
    char str[] = "Geeks-for-Geeks";
 
    // Returns first token
    char *token = strtok(str, "-");
    while (token != NULL)
    {
        printf("%s\n", token);
        token = strtok(NULL, "-");
    }
 
    return 0;
}

