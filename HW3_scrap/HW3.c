//
//  HW3.c
//
//
//  Created by Piyush Soni on 3/10/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
char *temp;
int main (int argc, char **argv){
    printf("Hello from p2, process id = () %d\n", getpid());
    for (int i =0; i<argc; i++){
        printf("%s\n", argv[i]);
    }
//    system(argv[0], argv[1], argv[2]);
    printf("Now, the child process will terminate\n");
    sprintf(temp, "%s %s", argv[1], argv[2]);
    system(temp);

    return 0;
}
