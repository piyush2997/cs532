//
//  p1.c
//  
//
//  Created by Piyush Soni on 3/13/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main (int argc, char **argv){
    printf("hello frommp1, proces id = () %d\n", getpid());
    char *args[]= {"Hello", "CS", "332", NULL };
    int pid = fork();
    int status;
    if (pid == 0){
        execl("./p2", args);
        printf("We are not supposed to see this");
    }
    else if (pid > 0){
        wait (&status);
    }
    
    printf("Ended parent");
    return 0;
}
