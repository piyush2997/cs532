#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

/*
Name: Jan Foksinski
BlazerId: jkf
Project #: Homework 2
To compile: gcc hw2.c
To run: ./<name of executable> <commands and arguments> <directory>
ex: ./executable -f txt -S -s 100 -t f ./projects/
*/
char *args[1024];
int array_counter = 0;
char * temp;
typedef struct
{
    int S_flag;
    int s_flag;
    int f_flag;
    int t_flag;
    int e_flag;
    int E_flag;
    int fileSize;
    char filterTerm[300];
    char fileType[2];
    char *unix;
} FlagArgs;

// type definition of the function pointer. It's void because it won't return anything
typedef void FileHandler(char *filePath, char *dirfile, FlagArgs flagArgs, int nestingCount);

// the function that will be used for this assignment
void myPrinterFunction(char *filePath, char *dirfile, FlagArgs flagArgs, int nestingCount)
{
    struct stat buf;       // buffer for data about file
    lstat(filePath, &buf); // very important that you pass the file path, not just file name
    char line[500];        // init some memory for the line that will be printed
    strcpy(line, "");      // verify a clean start
    strcat(line, dirfile); // init the line with the file name


    if (flagArgs.S_flag) // S case
    {
        char strsize[10];                          // allocate memory for the string format of the size
        sprintf(strsize, " %d", (int)buf.st_size); // assign the size to the allocated string
        strcat(line, strsize);                     // concatenate the line and the size
    }
    if (flagArgs.s_flag) // s case
    {
        if (flagArgs.fileSize > (int)buf.st_size) // if the file size is less than the expected
        {
            strcpy(line, ""); // clear the line print
        }
    }
    if (flagArgs.f_flag) // f case
    {
        if (strstr(dirfile, flagArgs.filterTerm) == NULL) // if the filter does not appear in the file
        {
            strcpy(line, ""); // clear the line print
        }
    }
    if (flagArgs.t_flag) // t case
    {
        if (strcmp(flagArgs.fileType, "f") == 0) // if the provided t flag is "f"
        {
            if (S_ISDIR(buf.st_mode) != 0) // if the file is a dir
            {
                strcpy(line, ""); // clear the line print
            }
        }
        if (strcmp(flagArgs.fileType, "d") == 0) // if the provided t flag is "d"
        {
            if (S_ISREG(buf.st_mode) != 0) // if the file is a regular file
            {
                strcpy(line, ""); // clear the line print
            }
        }
    }
    if (strcmp(line, "") != 0) // check to prevent printing empty lines
    {
        pid_t pid;
        int status;
        int i = 0;
        for (i = 0; i <= nestingCount; i++)
        {
            printf("\t");
        }
        printf("%s\n", line);
        
        pid = fork();
        if (flagArgs.e_flag)
        {
//            printf("%d\n", pid);
            
            if (pid == 0){
                args[1] = filePath;
                printf("%s\n", filePath);
                    for (int i =0; i<2; i++){
                        printf("%s\n", args[i]);
                    }
                
                
            }
//            else if (pid >0)
//            {
//                sprintf(flagArgs.unix, "%s %s", flagArgs.unix, dirfile);
//    //            printf("%s\n %s\n %s\n", dirfile, filePath, line);
//                printf("%s\n", flagArgs.unix);
//    //            system(flagArgs.unix);
//            }
            else if (pid > 0){
//                printf("this should be parent\n");
                wait (&status);
//                printf("Parent resumed\n");
            }
        }
        if (flagArgs.E_flag){
            
        }
    }
}

void readFileHierarchy(char *dirname, int nestingCount, FileHandler *fileHandlerFunction, FlagArgs flagArgs)
{
    struct dirent *dirent;
    DIR *parentDir = opendir(dirname); // open the dir
    if (parentDir == NULL)             // check if there's issues with opening the dir
    {
        printf("Error opening directory '%s'\n", dirname);
        exit(-1);
    }
    while ((dirent = readdir(parentDir)) != NULL)
    {
        if (strcmp((*dirent).d_name, "..") != 0 &&
            strcmp((*dirent).d_name, ".") != 0) // ignore . and ..
        {
            char pathToFile[300];
            sprintf(pathToFile, "%s/%s", dirname, ((*dirent).d_name));
            temp = (*dirent).d_name;
            fileHandlerFunction(pathToFile, (*dirent).d_name, flagArgs, nestingCount); // function pointer call
            
            if ((*dirent).d_type == DT_DIR)                                            // if the file is a dir
            {
                nestingCount++;                                                             // increase nesting before going in
                readFileHierarchy(pathToFile, nestingCount, fileHandlerFunction, flagArgs); // reccursive call
                nestingCount--;                                                             // decrease nesting once we're back
            }
        }
    }
    closedir(parentDir); // make sure to close the dir
}

int main(int argc, char **argv)
{

    // init opt :
    int opt = 0;
    // init a flag struct with 0s
    FlagArgs flagArgs = {
        .S_flag = 0,
        .s_flag = 0,
        .f_flag = 0,
        .t_flag = 0,
        .e_flag = 0,
        .E_flag = 0
    };

    
    while ((opt = getopt(argc, argv, "Ss:f:t:Ee:")) != -1)
    {
        switch (opt)
        {
        case 'S':
            flagArgs.S_flag = 1;
            break;

        case 's':
            flagArgs.s_flag = 1;
            flagArgs.fileSize = atoi(optarg);
            break;

        case 'f':
            flagArgs.f_flag = 1;
            strcpy(flagArgs.filterTerm, optarg);
            break;

        case 't':
            flagArgs.t_flag = 1;
            strcpy(flagArgs.fileType, optarg);
            break;
        case 'e':
            flagArgs.e_flag = 1;
            args[0] = flagArgs.unix = optarg;
            array_counter++;
            break;
        case 'E':
            flagArgs.E_flag = 1;
            args[0] = flagArgs.unix = optarg;
            array_counter++;
            break;
        }
    }
    
    if (opendir(argv[argc - 1]) == NULL) // check for if a dir is provided
    {
        char defaultdrive[300];
        getcwd(defaultdrive, 300);    // get the current working directory (if no directory was provided)
        printf("%s\n", defaultdrive); // prints the top-level dir
        readFileHierarchy(defaultdrive, 0, myPrinterFunction, flagArgs);
        return 0;
    }
    printf("%s\n", argv[argc - 1]); // prints the top-level dir
    readFileHierarchy(argv[argc - 1], 0, myPrinterFunction, flagArgs);
    
    return 0;
}
